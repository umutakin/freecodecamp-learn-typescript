let greetings: string = "hello umut";
console.log(greetings);

// number

let userId: number = 334455.6;
let userId2 = 334455.6;

// boolean

let isLoggedIn: boolean = false;

// any

let hero: string;

function getHero() {
    return "captain america";
}

hero = getHero();

export {};
