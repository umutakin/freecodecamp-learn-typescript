abstract class TakePhoto {
    constructor(public cameraMode: string, public filter: string) {}

    abstract getSephia(): void;
    getReelTime(): number {
        //some complex calculation
        return 8;
    }
}

const camera = new TakePhoto("", "");

class Instagramx extends TakePhoto {
    constructor(
        public cameraMode: string,
        public filter: string,
        public burst: number
    ) {
        super(cameraMode, filter);
    }

    getSephia(): void {
        console.log("sephia");
    }
}

const camera2 = new Instagramx("", "", Math.random());
camera2.getReelTime;
