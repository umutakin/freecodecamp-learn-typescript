// class User {
//     public email: string;
//     private name: string;
//     readonly city: string = "";

//     constructor(email: string, name: string) {
//         this.email = email;
//         this.name = name;
//     }
// }
class User {
    protected _courseCount = 1;

    readonly city: string = "";

    constructor(public email: string, public name: string) {}

    get getAppleEmail(): string {
        return `apple${this.email}`;
    }

    private deleteToken() {
        console.log("token deleted");
    }

    get getCourseCount(): number {
        return this._courseCount;
    }

    set setCourseCount(courseNum: number) {
        if (courseNum <= 1) {
            throw new Error("Course count should be more than 1");
        }

        this._courseCount = courseNum;
    }
}

class SubUser extends User {
    isFamily: boolean = true;

    changeCourseCount() {
        this._courseCount = 4;
    }
}

const umut = new User("u@u.com", "umutakin");
umut.city;
