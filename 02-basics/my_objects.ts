// const User = {
//     name: "umutakin",
//     email: "umutakin@fastmail.com",
//     isActive: true,
// };

// // function createUser({ name: string, isPaid: boolean }) {}

// // createUser({ name: "umutakin", isPaid: false, email: "umutakin@fastmail.com" });
// // createUser({ name: "umutakin", isPaid: false, email: "umutakin@fastmail.com" });

// function createCourse(): {} {
//     return { name: "svelte", price: 3.99 };
// }

// type User = {
//     name: string;
//     email: string;
//     isActive: boolean;
// };

// function createUser(user: User): User {
//     return user;
// }

// createUser({ name: "", email: "", isActive: true });

// let myUser = {
//     name: "x",
//     email: "x.x.com",
//     isActive: true,
// };

// createUser(myUser);

type User = {
    readonly _id: string;
    name: string;
    email: string;
    isActive: boolean;
    creditCardDetails?: number;
};

type CreditCardNumber = {
    cardnumber: string;
};

type CreditCardExpiryDate = {
    expiryDate: string;
};

type CreditCardDetails = CreditCardNumber &
    CreditCardExpiryDate & {
        cvv: number;
    };

let myUser: User = {
    _id: "12345",
    name: "u",
    email: "u@u.com",
    isActive: false,
};

myUser.email = "a@a.com";
// myUser._id = "45678";

export {};
