interface User {
    readonly dbId: number;
    email: string;
    userId: number;
    googleId?: string;
    // startTrial: () => string;
    startTrial(): string;
    getCoupon(couponName: string): number;
}

interface User {
    githubToken: string;
}

interface Admin extends User {
    role: string;
}

const umut: User = {
    dbId: 123,
    email: "u@u.com",
    userId: 1234,
    startTrial: () => {
        return "trial started";
    },
    getCoupon: (name: "umutakin") => {
        return 10;
    },
    githubToken: "token",
};

const akin: Admin = {
    dbId: 123,
    email: "u@u.com",
    userId: 1234,
    startTrial: () => {
        return "trial started";
    },
    getCoupon: (name: "umutakin") => {
        return 10;
    },
    githubToken: "token",
    role: "master",
};
