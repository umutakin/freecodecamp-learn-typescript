let score: number | string = 33;

score = 55;
score = "55";

type User = {
    name: string;
    id: number;
};

type Admin = {
    username: string;
    id: number;
};

let umutakin: User | Admin = {
    name: "umutakin",
    id: 334,
};

umutakin = {
    username: "ua",
    id: 334,
};

// function getDbId(id: number | string) {
//     console.log(`db id is : ${id}`);
// }

function getDbId(id: number | string) {
    if (typeof id === "string") {
        id.toLocaleLowerCase();
    }
    // id.toLowerCase();
    // console.log(`db id is : ${id}`);
}

getDbId(3);
getDbId("3");

const data: number[] = [1, 2, 3];
const data2: string[] = ["1", "2", "3"];
const data3: (string | number)[] = ["1", 2, "3 "];

let seatAllotment: "aisle" | "middle" | "window";

seatAllotment = "aisle";
seatAllotment = "other";
